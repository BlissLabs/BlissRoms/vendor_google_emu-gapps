GAPPS_PATH := $(dir $(LOCAL_PATH))proprietary/gapps

PRODUCT_COPY_FILES += \
    $(call find-copy-subdir-files,*,$(dir $(LOCAL_PATH))permissions/product,$(TARGET_COPY_OUT_PRODUCT))

PRODUCT_COPY_FILES += \
    $(call find-copy-subdir-files,*,$(dir $(LOCAL_PATH))permissions/system_ext,$(TARGET_COPY_OUT_SYSTEM_EXT))
