# android_vendor_google_emu-gapps
This is just the gapps extracted from Google's x86/x86_64 emulator images. 

## License
Please see [`LICENSE`](/LICENSE).
